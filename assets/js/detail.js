import countriesData from "../data/countriesData.js";

const container = document.getElementById("country-card");
const backBtn = document.getElementById("back-btn");
const countryContent = document.querySelector(".country-content");
const url = window.location.href;
const urlObj = new URL(url);
const searchParams = new URLSearchParams(urlObj.search);
let filteredCountries = [];

async function getCountriesData() {
  try {
    const data = await countriesData;
    return data;
  } catch (err) {
    console.log(err);
    return err;
  }
}

try {
  filteredCountries.push(await getCountriesData());
} catch (err) {
  console.log(err);
}

//Event listeners
backBtn.addEventListener("click", renderIndexPage);
countryContent.addEventListener("click", showCountryDetail);

function renderCountryDetail(filteredCountries) {
  const countryTitle = searchParams.get("countryName");
  let countryDetail = filteredCountries[0].filter(
    (country) => country.countryName === countryTitle
  );

  countryDetail = countryDetail[0];
  container.innerHTML = `
  <div class="col-md mb-5">
              <img
                src=${countryDetail.flag}
                class="country-flag"
                alt="country flag"
              />
            </div>
            <div class="col-md">
              <div class="country-detail">
                <h2 class="title m-0 mb-5 pb-3">${
                  countryDetail.countryName
                }</h2>

                <div class="row country-content-detail d-flex flex-column flex-md-row justify-content-between">
                  <div class="col country-native-detail mb-5">
                    <p class="detail-type">
                      <strong class="fw-400">Native Name: </strong> ${
                        countryDetail.nativeName
                      }
                    </p>
                    <p class="detail-type">
                      <strong class="fw-400">Population: </strong> ${
                        countryDetail.population
                      }
                    </p>
                    <p class="detail-type">
                      <strong class="fw-400">Region: </strong> ${
                        countryDetail.region
                      }
                    </p>
                    <p class="detail-type">
                      <strong class="fw-400">Sub Region: </strong> ${
                        countryDetail.subRegion
                      }
                    </p>
                    <p class="detail-type">
                      <strong class="fw-400">Capital: </strong> ${
                        countryDetail.capital
                      }
                    </p>
                  </div>
                  <div class="col country-domain-detail mb-5">
                    <p class="detail-type">
                      <strong class="fw-400">Top Level Domain: </strong>
                      ${countryDetail.topLevelDomain.join(", ")}
                    </p>
                    <p class="detail-type">
                      <strong class="fw-400">Currencies: </strong> ${countryDetail.currencies.join(
                        ", "
                      )}
                    </p>
                    <p class="detail-type">
                      <strong class="fw-400">Languages: </strong> ${countryDetail.languages.join(
                        ", "
                      )}
                    </p>
                  </div>
                </div>

                <div class="border-countries d-lg-flex align-items-center">
                  <p class="border-title m-0 pe-4 fs-3">Border Countries:</p>
                  <div class="border-btn-container d-flex justify-content-start flex-wrap" id="btn-container">
                  ${
                    countryDetail.borderCountries.length === 0
                      ? "No borders found."
                      : Array.from(countryDetail.borderCountries)
                          .map((text) => {
                            return `<button class="btn btn-white fs-6 shadow-sm">
                              ${text}
                            </button>`;
                          })
                          .join("")
                  }
                </div>
                </div>
              </div>
            </div>`;
}

function renderIndexPage() {
  window.location.href = "./index.html";
}

function showCountryDetail(e) {
  console.log(e.target);
  const countryTitle = (
    filteredCountries[0].filter(
      (country) => country.countryName === e.target.innerText
    )[0] || {}
  ).countryName;
  if (!countryTitle) return;
  window.location.href = `./detail.html?countryName=${countryTitle}`;
}

window.onload = renderCountryDetail(filteredCountries);
