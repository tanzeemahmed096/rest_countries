import fetchUrl from "../data/countriesData.js";

const container = document.querySelector("#country-container");
const formInput = document.querySelector("#form-input");
const regionName = document.querySelector(".region-select");
const form = document.getElementById("form");
let filteredCountries = [];

//Event listeners
form.addEventListener("submit", preventDefault);
regionName.addEventListener("click", showRegionData);
container.addEventListener("click", setCountryTitle);

async function getCountriesData() {
  try {
    const data = await fetchUrl;
    return data;
  } catch (err) {
    console.log(err);
    return err;
  }
}

async function renderData() {
  try {
    filteredCountries.push(await getCountriesData());
    renderCountryList(filteredCountries);
  } catch (err) {
    console.log(err);
  }
}

function preventDefault(e) {
  e.preventDefault();
  formInput.addEventListener("keyup", filterCountryOnInput);
}

function renderCountryList(filteredCountriesDetails) {
  const countriesDetails = [...filteredCountriesDetails[0]];
  container.innerHTML = "";
  for (let idx = 0; idx < countriesDetails.length; idx++) {
    const countryDetail = countriesDetails[idx];

    container.innerHTML += `
      <div class="col-md-6 col-lg-4 col-xl-3 mb-5 px-5 country">
      <div class="card shadow-sm border-0 data-title">
        <img
          src=${countryDetail.flag}
          class="card-img-top"
          style="height: 200px; object-fit: cover;"
          alt="country-flag"
        />
        <div class="card-body">
          <h4 class="card-title py-3 m-0">${countryDetail.countryName}</h4>
          <p class="card-text">
            <strong class="fw-400">Population: </strong>${countryDetail.population}
          </p>
          <p class="card-text card-region">
            <strong class="fw-400">Region: </strong>${countryDetail.region}
          </p>
          <p class="card-text">
            <strong class="fw-400">Capital: </strong>${countryDetail.capital}
          </p>
        </div>
      </div>
    </div>
    `;
  }
}

function filterCountryOnInput(e) {
  const inputVal = e.target.value;
  const countries = document.querySelectorAll(".country");

  Array.from(countries).forEach((country) => {
    const countryName = country.querySelector(".card-body").querySelector(".card-title").innerText;
    if (!countryName.toUpperCase().includes(inputVal.toUpperCase())) {
      country.style.display = "none";
    } else {
      country.style.display = "";
    }
  });

  regionName.value = "Find By Region";
}

function showRegionData(e) {
  formInput.value = "";
  const countries = document.querySelectorAll(".country");
  // regionName
  const inputVal = e.target.value;
  if (inputVal === "Find By Region") {
    Array.from(countries).forEach((country) => {
      country.style.display = "block";
    });
    return;
  }

  Array.from(countries).forEach((country) => {
    const countryName = country.querySelector(".card-body").querySelector(".card-region").innerText;
    if (!countryName.toUpperCase().includes(inputVal.toUpperCase())) {
      country.style.display = "none";
    } else {
      country.style.display = "";
    }
  });
}

function setCountryTitle(e) {
  if (e.target.closest(".data-title")) {
    const cardTitle =
      e.target.closest(".data-title").querySelector(".card-body").querySelector(".card-title").innerText;
    window.location.href = `./detail.html?countryName=${cardTitle}`;
  } else {
    return;
  }
}

window.onload = renderData;
