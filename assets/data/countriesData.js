async function fetchUrl() {
  try {
    const url = "https://restcountries.com/v3.1/all";
    const res = await fetch(url);
    const data = await res.json();
    return filterCountryDetails(data);
  } catch (err) {
    console.log(err);
    return err;
  }
}

function filterCountryDetails(countriesData) {
  const countryWithCodes = countriesData.reduce((acc, country) => {
    const countryCode = country.cca3;
    const countryName = country.name.common;
    acc[countryCode] = countryName;
    return acc;
  }, {});

  delete countryWithCodes.undefined;

  const filteredData = countriesData.reduce((acc, country) => {
    const flag = country.flags.svg;
    const countryName = country.name.common;
    const population = country.population.toLocaleString("en-US");
    const region = country.region;
    const capital = (country.capital || ["No Capital Found!!"])[0];
    const nativeName = Object.values(
      country.name.nativeName || { common: "No native name found" }
    ).reduce((acc, country) => {
      acc.push(country.common);
      return acc;
    }, [])[0];
    const subRegion = country.subregion;
    const topLevelDomain = country.tld;
    const currencies = Object.values(
      country.currencies || { name: "Currency Not Found" }
    ).reduce((acc, currency) => {
      acc.push(currency.name);
      return acc;
    }, []);
    const languages = Object.values(
      country.languages || ["No languages found."]
    );
    const borderCountries = (country.borders || [])
      .reduce((acc, countryCode) => {
        acc.push(countryWithCodes[countryCode]);
        return acc;
      }, [])
      .filter((country) => country);

    acc.push({
      flag,
      countryName,
      population,
      region,
      capital,
      nativeName,
      subRegion,
      topLevelDomain,
      currencies,
      languages,
      borderCountries,
    });
    return acc;
  }, []);

  return filteredData;
}

export default fetchUrl();
